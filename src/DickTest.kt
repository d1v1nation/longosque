import entitites.Dude
import world.World

/**
 * Created by cat on 03.07.17.
 */

fun main(args: Array<String>) {
    val inst = GameInstance(World())
    val p1 = Player()
    val p2 = Player()

    p1.connectTo(inst)
    p2.connectTo(inst)

    p1.interact { w ->
        w.setAt(0, 0, Dude())
    }
    p2.interact { w ->
        w.setAt(3, 3, Dude())
    }


    inst.world.commit()
}