import world.Action

/**
 * Created by cat on 03.07.17.
 */
class Player {
    var of: GameInstance? = null

    fun connectTo(g: GameInstance) {
        if (g != of) {
            of = g
            g.players.add(this)
        }
    }

    fun disconnect() {
        of?.players?.remove(this)
        of = null
    }

    fun interact(a: Action) {
        of?.world?.actionList?.add(a)
    }
}