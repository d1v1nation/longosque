package world

import entitites.Entity
import java.util.LinkedList

/**
 * Created by cat on 03.07.17.
 */
class World(var field: Field = Field(5)) {
    val actionList: LinkedList<Action> = LinkedList()

    fun setAt(x: Int, y: Int, e: Entity) {
        field.map[x][y].inhabitant = e
    }

    fun commit() {
        for (a in actionList) {
            a.invoke(this)
        }

        actionList.clear()
    }
}